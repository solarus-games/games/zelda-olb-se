local map = ...

function map:on_started()

  sol.timer.start(map, 500, function()
    map:set_entities_enabled("upper_panorama_tile", hero:get_layer() >= 5)
    return true
  end)
end
