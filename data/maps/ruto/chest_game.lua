-- Chest game of Ruto village.
local map = ...

local chest_game_manager = require("scripts/maps/chest_game_manager")

chest_game_manager:create(map, "ruto_chest_game_piece_of_heart")