local map = ...
local game = map:get_game()

function leader:on_interaction()
  local mine_opening_quest = game:get_value("mine_opening_quest")

  if mine_opening_quest == 5 then
    game:start_dialog("ruto.leader_house.deal")
  elseif mine_opening_quest == 4 then
    local bribe = 10
    game:start_dialog("ruto.leader_house.someone_locked", bribe, function(answer)
      if answer == 3 then -- ok
        if game:get_money() < bribe then
          game:start_dialog("not_enough_money")
        else
          game:remove_money(bribe)
          game:set_value("mine_opening_quest", 5)
          game:start_dialog("ruto.leader_house.deal")
        end
      end
    end)
  else
    game:start_dialog("ruto.leader_house.no_talking_to_you")    
  end
end