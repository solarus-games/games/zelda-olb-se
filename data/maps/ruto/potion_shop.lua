local map = ...
local game = map:get_game()

function map:on_started()
  merchant:get_sprite():set_animation("reading")
end

function merchant:on_interaction()
  local bottle_variant_red = 2
  local bottle_variant_green = 3

  local first_empty_bottle = game:get_first_empty_bottle()

  local function buy_potion(bottle_variant, price)
    game:start_dialog("ruto.potion_shop.offer_potion_" .. bottle_variant, price, function(answer)
      if answer == 2 then -- yes
        if game:get_money() < price then
          game:start_dialog("not_enough_money")
        else
          game:remove_money(price)
          hero:start_treasure(first_empty_bottle:get_name(), bottle_variant)
        end
      end
    end)
  end

  if first_empty_bottle == nil then
    game:start_dialog("ruto.potion_shop.no_bottle")
  else
    game:start_dialog("ruto.potion_shop.offer", price, function(answer)
      if answer == 2 then -- Red potion
        buy_potion(bottle_variant_red, 60)
      elseif answer == 3 then -- Green potion
        buy_potion(bottle_variant_green, 60) -- TODO verify price
      end
    end)
  end
end
