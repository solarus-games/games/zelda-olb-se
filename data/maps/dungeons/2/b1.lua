local map = ...
local game = map:get_game()

function map:on_started(destination)

  if game:get_value("d2_b1_prison_npc_key_1") then
    prison_npc_1:remove()
  end
  if game:get_value("d2_b1_prison_npc_key_2") then
    prison_npc_2:remove()
  end
  if game:get_value("d2_b1_prison_npc_key_3") then
    prison_npc_3:remove()
  end
end

local function prison_npc_interaction(num)
  game:start_dialog("dungeons.2.prison_npc.thanks", function()
    game:start_dialog("dungeons.2.prison_npc." .. num, function()
      game:start_dialog("dungeons.2.prison_npc.take_this", function()
        game:set_value("d2_b1_prison_npc_key_" .. num, true)
        hero:start_treasure("small_key")
      end)
    end)
  end)
end

function prison_npc_1:on_interaction()
  prison_npc_interaction(1)
end

function prison_npc_2:on_interaction()
  prison_npc_interaction(2)
end

function prison_npc_3:on_interaction()
  prison_npc_interaction(3)
end