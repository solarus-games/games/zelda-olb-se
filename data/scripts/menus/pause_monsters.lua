local monsters_manager = {}

local gui_designer = require("scripts/menus/lib/gui_designer")

local models = {
  { enemy = "alttp/popo",                page = 1, frame = { 152,   8, 32, 32 }, xy = {  8, 13 } },
  { enemy = "alttp/zol_green",           page = 1, frame = { 208,   8, 32, 32 }, xy = {  8, 13 } },
  { enemy = "alttp/crow",                page = 1, frame = { 264,   8, 32, 32 }, xy = {  8, 13 } },
  { enemy = "alttp/octorok",             page = 1, frame = {  16,  48, 48, 48 }, xy = { 16, 21 } },
  { enemy = "alttp/zora_water",          page = 1, frame = {  96,  48, 48, 48 }, xy = { 16, 21 } },
  { enemy = "alttp/zora_feet",           page = 1, frame = { 176,  48, 48, 48 }, xy = { 16, 25 } },
  { enemy = "alttp/pikku",               page = 1, frame = { 256,  48, 48, 48 }, xy = { 16, 25 } },
  { enemy = "alttp/stalfos_blue",        page = 1, frame = {  16, 112, 48, 48 }, xy = { 16, 25 } },
  { enemy = "alttp/armos",               page = 1, frame = {  96, 112, 48, 48 }, xy = { 16, 29 } },
  { enemy = "alttp/moblin",              page = 1, frame = { 176, 112, 48, 48 }, xy = { 16, 25 } },
  { enemy = "alttp/wizzrobe_white",      page = 1, frame = { 256, 112, 48, 48 }, xy = { 16, 25 } },
  { enemy = "alttp/lynel",               page = 1, frame = {  16, 176, 48, 48 }, xy = { 16, 29 } },
  { enemy = "alttp/tektite_blue",        page = 1, frame = {  96, 176, 48, 48 }, xy = { 16, 21 } },
  { enemy = "alttp/vulture",             page = 1, frame = { 176, 176, 48, 48 }, xy = { 16, 21 } },
  { enemy = "alttp/geldman",             page = 1, frame = { 252, 176, 56, 48 }, xy = { 20, 21 } },

  { enemy = "alttp/keese",               page = 2, frame = { 152,   8, 32, 32 }, xy = {  8, 13 } },
  { enemy = "alttp/chasupa",             page = 2, frame = { 208,   8, 32, 32 }, xy = {  8, 13 } },
  { enemy = "alttp/rope",                page = 2, frame = { 264,   8, 32, 32 }, xy = {  8, 13 } },
  { enemy = "alttp/zazak_blue",          page = 2, frame = {  16,  48, 48, 48 }, xy = { 16, 21 } },
  { enemy = "alttp/hinox",               page = 2, frame = {  96,  48, 48, 48 }, xy = { 16, 21 } },
  { enemy = "alttp/hover",               page = 2, frame = { 176,  48, 48, 48 }, xy = { 16, 21 } },
  { enemy = "alttp/gibdo",               page = 2, frame = { 256,  48, 48, 48 }, xy = { 16, 21 } },
  { enemy = "alttp/ropa",                page = 2, frame = {  16, 112, 48, 48 }, xy = { 16, 25 } },
  { enemy = "alttp/bari_blue",           page = 2, frame = {  96, 112, 48, 48 }, xy = { 16, 25 } },
  { enemy = "alttp/wizzrobe_blue",       page = 2, frame = { 176, 112, 48, 48 }, xy = { 16, 25 } },
  { enemy = "alttp/sand_crab",           page = 2, frame = { 256, 112, 48, 48 }, xy = { 16, 21 } },
  { enemy = "alttp/freezor",             page = 2, frame = {  16, 176, 48, 48 }, xy = { 16, 21 } },
  { enemy = "alttp/stalfos_red",         page = 2, frame = {  96, 176, 48, 48 }, xy = { 16, 25 } },
  { enemy = "alttp/pengator",            page = 2, frame = { 176, 176, 48, 48 }, xy = { 16, 25 } },
  { enemy = "som/ghoul",                 page = 2, frame = { 256, 176, 48, 48 }, xy = { 16, 25 } },

  { enemy = "alttp/stal",                page = 3, frame = { 152,   8, 32, 32 }, xy = {  8, 13 } },
  { enemy = "alttp/poe",                 page = 3, frame = { 208,   8, 32, 32 }, xy = {  8, 13 } },
  { enemy = "alttp/hyu",                 page = 3, frame = { 264,   8, 32, 32 }, xy = {  8, 13 } },
  { enemy = "alttp/goriya_green",        page = 3, frame = {  16,  48, 48, 48 }, xy = { 16, 25 } },
  { enemy = "alttp/eyegore_green",       page = 3, frame = {  96,  48, 48, 48 }, xy = { 16, 25 } },
  { enemy = "alttp/goriya_red",          page = 3, frame = { 176,  48, 48, 48 }, xy = { 16, 25 } },
  { enemy = "alttp/eyegore_red",         page = 3, frame = { 256,  48, 48, 48 }, xy = { 16, 25 } },
  { enemy = "alttp/stalfos_grey",        page = 3, frame = {  16, 112, 48, 48 }, xy = { 16, 25 } },
  { enemy = "alttp/tarosu_blue",         page = 3, frame = {  96, 112, 48, 48 }, xy = { 16, 25 } },
  { enemy = "alttp/tarosu_red",          page = 3, frame = { 176, 112, 48, 48 }, xy = { 16, 25 } },
  { enemy = "alttp/sword_soldier_green", page = 3, frame = { 256, 112, 48, 48 }, xy = { 16, 25 } },
  { enemy = "alttp/sword_soldier_blue",  page = 3, frame = {  16, 176, 48, 48 }, xy = { 16, 25 } },
  { enemy = "alttp/spear_soldier_red",   page = 3, frame = {  96, 176, 48, 48 }, xy = { 16, 25 } },
  { enemy = "alttp/oni_link",            page = 3, frame = { 176, 176, 48, 48 }, xy = { 16, 25 } },
  
  { enemy = "alttp/blind",               page = 4, frame = { 144,   8, 64, 56 }, xy = { 24, 37 } },
  { enemy = "alttp/helmasaur_king",      page = 4, frame = { 224,   8, 80, 88 }, xy = { 32, 63 } },
  { enemy = "som/tropicallo",            page = 4, frame = {  16,  48, 64, 80 }, xy = { 24, 62 } },
  { enemy = "som/kilroy",                page = 4, frame = {  16, 144, 64, 80 }, xy = { 24, 62 } },
  { enemy = "soe/aquagoth",              page = 4, frame = {  96,  80,112,144 }, xy = { 48,118 } },
  { enemy = "alttp/stalfos_knight",      page = 4, frame = { 240, 104, 48, 56 }, xy = { 16, 36 } },
  { enemy = "alttp/armos_knight",        page = 4, frame = { 240, 168, 48, 56 }, xy = { 16, 36 } },
  
  { enemy = "som/vampire",               page = 5, frame = {  16,  48, 72, 82 }, xy = { 28, 52 } },
  { enemy = "som/minotaur",              page = 5, frame = {  96,  48, 72, 82 }, xy = { 28, 58 } },
  { enemy = "alttp/mothula",             page = 5, frame = { 200,  16, 80, 48 }, xy = { 32, 16 } },
  { enemy = "alttp/arrghus",             page = 5, frame = {  20, 148, 48, 65 }, xy = { 16, 24 } },
  { enemy = "som/spikey_tiger",          page = 5, frame = {  82, 144, 72, 80 }, xy = { 28, 56 } },
  { enemy = "alttp/kholdstare",          page = 5, frame = { 184,  88, 48, 48 }, xy = { 16, 29 } },
  { enemy = "alttp/vitreous",            page = 5, frame = { 248,  88, 48, 48 }, xy = { 16, 21 } },
  { enemy = "alttp/agahnim",             page = 5, frame = { 168, 160, 48, 48 }, xy = { 16, 29 } },
  { enemy = "alttp/ganon",               page = 5, frame = { 232, 160, 64, 48 }, xy = { 26, 31 } },
}
local num_pages = 5

function monsters_manager:new(game)

  local monsters = {}

  local layout
  local page = 1

  local function build_layout(page)

    layout = gui_designer:create(320, 240)
    layout:make_green_tiled_background()

    layout:make_big_wooden_frame(16, 8, 112, 32)
    layout:make_text(sol.language.get_string("pause.monsters.title") .. " " .. page, 72, 16, "center")

    for _, monster in ipairs(models) do
      if monster.page == page then
        layout:make_wooden_frame(unpack(monster.frame))
        if game:get_value("monsters_encyclopedia_" .. monster.enemy:gsub( "/", "_")) then
          local sprite = sol.sprite.create("enemies/" .. monster.enemy)
          if sprite:has_animation("walking") then
            sprite:set_animation("walking")
            sprite:set_direction(3)
            sprite:set_paused(true)  -- TODO isn't this better when animated?
            local dst_x = monster.frame[1] + monster.xy[1] + 8
            local dst_y = monster.frame[2] + monster.xy[2] + 8
            layout:make_sprite(sprite, dst_x, dst_y)
          end
        end
      end
    end
  end

  build_layout(page)

  function monsters:on_command_pressed(command)

    local handled = false
    if command == "up" then
      if page > 1 then
        sol.audio.play_sound("cursor")
        page = page - 1
        build_layout(page)
        handled = true
      end
    elseif command == "down" then
      if page < num_pages then
        sol.audio.play_sound("cursor")
        page = page + 1
        build_layout(page)
        handled = true
      end
    end
    return handled
  end

  function monsters:on_draw(dst_surface)

    layout:draw(dst_surface)
  end

  function monsters:get_monster_count()

    local monster_count = 0
    for _, monster in ipairs(models) do
      if game:get_value("monsters_encyclopedia_" .. monster.enemy) then
        monster_count = monster_count + 1
      end
    end
    return monster_count
  end

  function monsters:get_max_count()

    return #models
  end

  return monsters
end

return monsters_manager
