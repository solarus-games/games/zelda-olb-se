-- Provides additional features to the item type for this quest.

local item_meta = sol.main.get_metatable("item")

-- Called when the player obtains the item.
item_meta:register_event("on_obtained", function(item)

  -- Give the magic bar if necessary.
  local magic_bar = item:get_game():get_item("magic_bar")
  if item.is_magic and not magic_bar:has_variant() then
    magic_bar:set_variant(1)
  end
end)

local chest_meta = sol.main.get_metatable("chest")
-- Called when the player obtains the item in a chest.
chest_meta:register_event("on_opened", function(chest, item, variant, savegame_variable)
  game = chest:get_game()
  hero = game:get_hero()

  -- progressive items
  if item:get_name() == "tunic" then
    current = game:get_ability("tunic")
    variant = current + 1
  elseif item:get_name() == "sword" then
    current = game:get_ability("sword")
    variant = current + 1
  elseif item:get_name() == "shield" then
    current = game:get_ability("shield")
    variant = current + 1
  elseif item:get_name() == "glove" then
    current = game:get_ability("lift")
    variant = current + 1
  end
  hero:start_treasure(item:get_name(), variant)
end)