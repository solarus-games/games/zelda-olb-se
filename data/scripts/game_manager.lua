-- Script that creates a game ready to be played.

-- Usage:
-- local game_manager = require("scripts/game_manager")
-- local game = game_manager:create("savegame_file_name")
-- game:start()

require("scripts/multi_events")
local initial_game = require("scripts/initial_game")

local game_manager = {}

local function process_command(game, command)

  local function is_command(command, name)
    local trigger = false

    if command == game:get_value("keyboard_" .. name) then
      trigger = true
    elseif command == game:get_value("joypad_" .. name) then
      trigger = true
    end

    return trigger
  end

  if game.customizing_command then
    -- Don't treat this input normally, it will be recorded as a new command binding
    -- by the commands menu.
    return false
  end

  local hero = game:get_hero()
  local handled = false

  if game:is_pause_allowed() then  -- Commands below are menus.
    if is_command(command, "map") then
      -- Map menu.
      if not game:is_suspended() or game:is_paused() then
        game:switch_pause_menu("map")
        handled = true
      end

    elseif is_command(command, "monsters") then
      -- Monsters menu.
      if not game:is_suspended() or game:is_paused() then
        if game:has_item("monsters_encyclopedia") then
          game:switch_pause_menu("monsters")
          handled = true
        end
      end

    elseif is_command(command, "commands") then
      -- Commands menu.
      if not game:is_suspended() or game:is_paused() then
        game:switch_pause_menu("commands")
        handled = true
      end

    elseif is_command(command, "save") then
      -- Save menu
      if not game:is_paused() and
          not game:is_dialog_enabled() and
          game:get_life() > 0 then
        game:start_dialog("save_quit", function(answer)
          if answer == 2 then
            -- Continue.
            sol.audio.play_sound("danger")
          elseif answer == 3 then
            -- Save and quit.
            sol.audio.play_sound("quit")
            game:save()
            sol.main.reset()
          else
            -- Quit without saving.
            sol.audio.play_sound("quit")
            sol.main.reset()
          end
        end)
        handled = true
      end
    elseif hero:get_state() == "free" then -- Commands below are actions
      if is_command(command, "teleport") then
        -- Teleport.
        if game:is_onilink() and game:get_dungeon() and hero:get_state() == "free" then
          -- TODO sol.audio.play_sound("teleport")
          hero:teleport(game:get_starting_location())
          handled = true
        end
      end
    end
  end

  return handled

end

-- Creates a game ready to be played.
function game_manager:create(file)

  -- Create the game (but do not start it).
  local exists = sol.game.exists(file)
  local game = sol.game.load(file)
  if not exists then
    -- This is a new savegame file.
    initial_game:initialize_new_savegame(game)
  end

  -- Function called when the player presses a key during the game.
  game:register_event("on_key_pressed", function(game, key)
    return process_command(game, key)
  end)

  -- Function called when the player presses a joypad button during the game.
  game:register_event("on_joypad_button_pressed", function(game, button)
    return process_command(game, "button " .. button)
  end)

  return game
end

return game_manager
