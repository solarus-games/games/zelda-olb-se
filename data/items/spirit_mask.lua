local item = ...
local game = item:get_game()

-- Two variants:
-- - Empty
-- - With Oni-Link's substance

function item:on_created()
  item:set_savegame_variable("possession_spirit_mask")
  item:set_assignable(true)
end

function item:on_variant_changed(variant)
  game:refresh_hero_sprites()
end
